apitulo primero del prolego do comj
ença su obra de titus liujus Et di
sen asy
y yo me pongo a escriujr las cosas
fechans por los Romanos desdel co
mienço que Roma fue fundada e
non so çierto si sera cosa conuenjble
Et si yo lo sabre o lo osare dezjr por que yo
veo que esto es vna cosa que por los ançia
nos ha estada escripta et publicada X por
quanto los muchos escriuanos delas estorias
cuydan et son en opinion de contramas çierta
et mas verdadera mente algunas cosas delas
pasadas e la Rudeza de la antigua materia
declara et afirmar et asumar por lenguajes
artifiçiores et prouer en ello con plazeria a
yudar et consejar por mi parrtida a tener en
mejoria los fechos de aquel pueblo que es prinçe
pe delas tierras /Ca sy la mj obra es pequena
a conparaçion de otros escriptores /o ystoriadores
que ante mj han escriujdo et maguer mj fama
sea obscura o chican yo me esforçare enla gr̄a
deza e nobleza dellos et non me desplazera
ze yo fuere poco presçiado a respleyto de
asy so buenas psonas que enesta materia ayan
escriujdo ¶ Ca yo toda via cosa es de gran
trabajo por que es forçado et conbiene Repe
tir et escriuir las cosas que fueron e acaesçieron
et son fechans de sieteçienntos años fasta a
qui ¶ las quales tanto son çiençias de peque
ños començamjentos que apresende grandesa
Et asy yo çier
de aqłłos X
to Et non pongo en dubda que contar los prime
ros comjenços deste pueblo et las cosas que
fueron çerca de su començamjento sera muydele
itable aquellos quelas leyeren mas que serian las
cosas deste tienpo presente en el qual las vertu
des et las fuerças deste pueblo son asy gṙa
des et asi fierras que ellas consumen et des
gastan asy mesmas ¶ este sera el gua
lardon de mj trabajo que yo demandare e queero
dezjr que en Repitiendo et Recontando las
cosas antiguas yo sere contado de aquellos
que por muchos años auemos visto algus
cosas en nuestras hedades ¶
libre de todos cuydados
mj coraçon et de mj pensamjento todos los ot
Negoçios et yo so çierto que cuydados et pen
samjentos non fazen el coraçon enflaqueçer dezjr
la verdat enpero toda vna le turuan e lo mu
euen.